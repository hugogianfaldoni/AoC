#include <iostream>
#include <cmath>
#include <sstream>

int main() {
  std::string line;

  std::getline(std::cin, line);
  std::stringstream ss(line);

  std::string token;
  int x = 0, y = 0;
  int dir = 0;
  while (std::getline(ss, token, ' ')) {
    if (token[0] == 'R')
      dir++;
    else
      dir--;
    if (dir < 0) dir += 4;
    dir %= 4;

    int d;
    sscanf(token.c_str(), "%*c%d", &d);
    if (dir == 1) 
      x += d;
    else if (dir == 3) 
      x -= d;
    else if (dir == 0) 
      y += d;
    else if (dir == 2) 
      y -= d;
  }

  std::cout << std::abs(x) + std::abs(y) << std::endl;

  return 0;
}
