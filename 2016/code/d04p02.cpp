#include <iostream>
#include <algorithm>
#include <vector>
#include <numeric>

std::string get_checksum(std::string const& line) {
  auto pos = line.find('[');
  // all checksums are 5 character long
  return line.substr(pos + 1, 5);
}

int get_ID(std::string const& line) {
  auto pos = line.rfind('-');
  // all IDs are 3 char long
  return std::stoi(line.substr(pos+1, 3));
}

std::string get_string(std::string const& line) {
  auto pos = line.rfind('-');
  std::string str(line.substr(0, pos));
  str.erase(std::remove(str.begin(), str.end(), '-'), str.end());

  return str;
}

std::string build_checksum(std::string const& line) {
  std::vector<int> count(26);

  for (auto const& c: line) {
    count[c - 'a']++;
  }

  std::string sum;
  for (size_t i = 0; i < 5; i++) {
    char cmax;
    int max = -1, imax;
    for (size_t j = 0; j < 26; j++) {
      if (count[j] > max) {
        cmax = j + 'a';
        max = count[j];
        imax = j;
      }
    }
    sum += cmax;
    count[imax] = -1;
  }

  return sum;
}

std::string get_room_name(int shift, std::string const& line) {
  auto pos = line.rfind('-');
  std::string str(line.substr(0, pos));

  for (auto& c: str) {
    if (c != '-') {
      int d = (c - 'a' + shift)%26;
      c = d + 'a';
    }
  }

  return str;
} 

int main() {
  std::string line;

  while (std::getline(std::cin, line)) {
    if (line.empty())
      continue;

    std::string checksum = get_checksum(line);
    if (checksum == build_checksum(get_string(line))) {
      int shift = get_ID(line);
      std::string name = get_room_name(shift, line);
      if (name.find("northpole") != std::string::npos) {
        std::cout << shift << std::endl;
        break;
      }
    }
  }
  
  return 0;
}
