#include <iostream>
#include <cmath>
#include <sstream>
#include <set>

int main() {
  std::string line;

  std::getline(std::cin, line);
  std::stringstream ss(line);

  std::set<std::pair<int, int>> coord;

  std::string token;
  int x = 0, y = 0;
  int dir = 0;
  while (std::getline(ss, token, ' ')) {
    if (token[0] == 'R')
      dir++;
    else
      dir--;
    if (dir < 0) dir += 4;
    dir %= 4;

    int d;
    sscanf(token.c_str(), "%*c%d", &d);
    while (d--) {
      if (dir == 1) 
        x++;
      else if (dir == 3) 
        x--;
      else if (dir == 0) 
        y++;
      else if (dir == 2) 
        y--;

      if (!coord.insert({x, y}).second) {
        std::cout << std::abs(x) + std::abs(y) << std::endl;
        return 0;
      }
    }
  }

  return 0;
}
