#include <iostream>
#include <vector>
#include <map>

template<class T, class U>
T key_max_value(std::map<T, U> const& map) {
  T imax = map.begin()->first;
  U max = map.begin()->second;
  for (auto const& v: map) {
    if (v.second > max) {
      imax = v.first;
      max = v.second;
    }
  }

  return imax;
}

int main() {
  std::string line;
  std::getline(std::cin, line);
    
  std::vector<std::map<char, int>> freq(line.size());

  do {
    if (line.empty())
      continue;

    for (size_t i = 0; i < line.size(); i++) {
      freq[i][line[i]]++;
    }
  }
  while (std::getline(std::cin, line));

  for (auto& m: freq) {
    std::cout << key_max_value(m);
  }
  std::cout << std::endl;

  return 0;
}
