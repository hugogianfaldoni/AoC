#include <iostream>
#include <numeric>
#include <array>

#include <openssl/md5.h>

int main() {
  std::string ID;
  std::getline(std::cin, ID);

  std::array<int, 8> pass;
  pass.fill(-1);

  size_t i, count;
  for (i = 0, count = 0; count < 8 ; i++) {
    std::string str(ID + std::to_string(i));
    std::array<unsigned char, 16> digest;
    MD5((const unsigned char*)str.c_str(), str.size(), digest.data());
    if (!digest[0] && !digest[1] && digest[2] < 8) {
      if (pass[digest[2]] < 0) {
        std::cout << (int)digest[2] << ": " << std::hex << ((digest[3]&0xf0)>>4) << std::endl;;
        pass[digest[2]] = (digest[3]&0xf0)>>4;
        count++;
      }
    }
  }
  std::cout << std::dec << i << std::endl;

  for (auto& v: pass)
    std::cout << std::hex << v;
  std::cout << "\n";

  return 0;
}
