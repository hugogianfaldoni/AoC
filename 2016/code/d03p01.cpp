#include <iostream>
#include <sstream>

int main() {
  std::string line;

  int count = 0;
  while (std::getline(std::cin, line)) {
    std::stringstream ss(line);
    int a(0), b(0), c(0);
    ss >> a >> b >> c;
    if ((a + b > c) && (a + c > b) && (b + c > a))
      count++;
  }

  std::cout << "Correct triangles: " << count << std::endl;
  return 0;
}
