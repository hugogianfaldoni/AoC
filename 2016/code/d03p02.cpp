#include <iostream>
#include <sstream>
#include <vector>
#include <array>

bool check(int a, int b, int c) {
  return ((a + b > c) && (a + c > b) && (b + c > a));
}

int main() {
  std::string line;

  int count = 0;
  int lcount = 0;
  std::array<std::array<int, 3>, 3> arr;

  while (std::getline(std::cin, line)) {
    std::stringstream ss(line);
    int a, b, c;
    ss >> a >> b >> c;
    arr[lcount++] = {a, b, c};

    if (lcount == 3) {
      for (size_t i = 0; i < 3; i++) {
        if (check(arr[0][i], arr[1][i], arr[2][i]))
          count++;
      }

      arr = {};
      lcount = 0;
    }
  }

  std::cout << "Correct triangles: " << count << std::endl;
  return 0;
}
