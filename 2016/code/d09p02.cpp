#include <iostream>
#include <sstream>

unsigned long long decompress(std::string const& data) {
  if (data.empty()) return 0;

  if (data[0] == '(') {
    auto xpos = data.find('x');
    auto ppos = data.find(')');
    int len = std::stoi(data.substr(1, xpos));
    int rep = std::stoi(data.substr(xpos+1, ppos - xpos));

    std::string d = data.substr(ppos+1, len);

    return decompress(d)*rep + decompress(data.substr(ppos+1+len));
  }
  else {
    auto ppos = data.find('(');
    if (ppos == std::string::npos)
      return data.size();
    return ppos + decompress(data.substr(ppos));
  }
}

int main() {
  std::string input;
  std::getline(std::cin, input);


  std::cout << decompress(input) << std::endl;
  return 0;
}
