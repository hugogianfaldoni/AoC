#include <iostream>
#include <set>

int main() {
  std::string line;

  int count = 0;
  while (std::getline(std::cin, line)) {
    bool hyper = false;
    std::set<std::pair<char, char>> aba, bab;
    for (const char* s = line.c_str(); *(s + 2) != '\0'; s++) {
      if (*s == '[') hyper = true;
      else if (*s == ']') hyper = false;
      else if (s[0] == s[2] && s[0] != s[1]) {
        if (hyper)  bab.emplace(s[1], s[0]);
        else        aba.emplace(s[0], s[1]);
      }
    }

    for (auto const& p: aba) {
      if (bab.find(p) != bab.end()) {
        count++;
        break;
      }
    }
  }
  
  std::cout << count << std::endl;

  return 0;
}
