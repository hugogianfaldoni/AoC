#include <iostream>
#include <algorithm>

int main() {
  char pad[3][3] =
  {
    {'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'}
  };

  int x = 1, y = 1;
  std::string code;

  std::string line;
  while (std::getline(std::cin, line)) {
    if (line.empty()) continue;
    for (auto& c: line) {
      switch (c) {
        case 'U':
          y = std::clamp(y-1, 0, 2);
          break;
        case 'D':
          y = std::clamp(y+1, 0, 2);
          break;
        case 'L':
          x = std::clamp(x-1, 0, 2);
          break;
        case 'R':
          x = std::clamp(x+1, 0, 2);
          break;
      }
    }
    code += pad[y][x];
  }
  std::cout << code << std::endl;

  return 0;
}
