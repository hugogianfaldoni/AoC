#include <iostream>
#include <sstream>
#include <array>

#include <openssl/md5.h>

int main() {
  std::string ID;
  std::getline(std::cin, ID);

  std::stringstream pass;

  for (size_t i = 0, count = 0; count < 8 ; i++) {
    std::string str(ID + std::to_string(i));
    std::array<unsigned char, 16> digest;
    MD5((const unsigned char*)str.c_str(), str.size(), digest.data());
    if (!digest[0] && !digest[1] && digest[2] < 16) {
      pass << std::hex << (int)digest[2];
      count++;
    }
  }

  std::cout << pass.str() << std::endl;
  return 0;
}
