#include <iostream>
#include <vector>
#include <array>
#include <sstream>

int main() {
  const int width = 50, height = 6;
  std::array<std::array<bool, height>, width> grid{{}};

  auto print = [&]() {
    for (size_t i = 0; i < height; i++) {
      for (size_t j = 0; j < width; j++) {
        if (grid[j][i])   std::cout << "#";
        else              std::cout << ".";
      }
      std::cout << "\n";
    }
    std::cout << "\n";
  };

  auto rect = [&](int w, int h) {
    for (int i = 0; i < w; i++) {
      for (int j = 0; j < h; j++) {
        grid[i%width][j%height] = true;
      }
    }
  };

  auto shift_down = [&](int col, int shift) {
    shift %= height;
    std::vector<bool> saved;
    // save the n last bits
    for (size_t i = height - shift; i < height; i++)
      saved.push_back(grid[col][i]);
    // shift bits
    for (size_t i = height-1; i >= shift; i--)
      grid[col][i] = grid[col][i-shift];
    // put back saved bits
    for (size_t i = 0; i < saved.size(); i++) 
      grid[col][i] = saved[i];
  };

  auto shift_right = [&](int row, int shift) {
    shift %= width;
    std::vector<bool> saved;
    // save the n last bits
    for (size_t i = width - shift; i < width; i++)
      saved.push_back(grid[i][row]);
    // shift bits
    for (size_t i = width-1; i >= shift; i--)
      grid[i][row] = grid[i-shift][row];
    // put back saved bits
    for (size_t i = 0; i < saved.size(); i++) 
      grid[i][row] = saved[i];
  };

  std::string line;

  while (std::getline(std::cin, line)) {
    if (line.empty()) continue;
    std::stringstream ss(line);
    std::string op;
    ss >> op;
    if (op == "rect") {
      std::string dim;
      ss >> dim;
      auto pos = dim.find('x');
      rect(std::stoi(dim.substr(0, pos)), std::stoi(dim.substr(pos+1)));
    }
    else {
      std::string dir, tmp;
      ss >> dir >> tmp;
      int pos = std::stoi(tmp.substr(tmp.find('=')+1));
      ss >> tmp;
      int shift;
      ss >> shift;
      if (dir == "column")
        shift_down(pos, shift);
      else 
        shift_right(pos, shift);
    }
  }

  print();
  return 0;
}
