#include <iostream>
#include <algorithm>

int main() {
  char pad[5][5] =
  {
    {' ', ' ', '1', ' ', ' '},
    {' ', '2', '3', '4', ' '},
    {'5', '6', '7', '8', '9'},
    {' ', 'A', 'B', 'C', ' '},
    {' ', ' ', 'D', ' ', ' '}
  };

  int x = 2, y = 0;
  std::string code;

  std::string line;
  while (std::getline(std::cin, line)) {
    if (line.empty()) continue;
    for (auto& c: line) {
      int nX=x, nY=y;
      switch (c) {
        case 'U':
          nY = std::clamp(y-1, 0, 4);
          break;
        case 'D':
          nY = std::clamp(y+1, 0, 4);
          break;
        case 'L':
          nX = std::clamp(x-1, 0, 4);
          break;
        case 'R':
          nX = std::clamp(x+1, 0, 4);
          break;
      }
      if (pad[nY][nX] != ' ') {
        x = nX;
        y = nY;
      }
    }
    code += pad[y][x];
  }
  std::cout << code << std::endl;

  return 0;
}
