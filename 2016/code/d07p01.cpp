#include <iostream>

int main() {
  std::string line;

  int count = 0;
  while (std::getline(std::cin, line)) {
    bool hyper = false;
    bool abba = false;
    for (const char* s = line.c_str(); *(s + 3) != '\0'; s++) {
      if (*s == '[') hyper = true;
      else if (*s == ']') hyper = false;
      else if (s[0] == s[3] && s[1] == s[2] && s[0] != s[1]) {
        if (hyper) {
          abba = false;
          break;
        }
        else {
          abba = true;
        }      
      }
    }
    count += abba;
  }
  
  std::cout << count << std::endl;

  return 0;
}
