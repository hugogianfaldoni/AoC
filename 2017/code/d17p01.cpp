#include <iostream>
#include <vector>

int main() {
  int step;
  std::cin >> step;
  std::vector<int> buffer;
  buffer.reserve(2018);

  auto saved = buffer.begin();
  buffer.insert(buffer.begin(), 0);
  for (size_t i = 1, pos = 0; i < 2018; i++) {
    pos = (pos + step + 1)%buffer.size();
    buffer.insert(buffer.begin()+pos+1, i);
    if (i == 2017)
      saved += pos+2;
  }

  std::cout << *saved << std::endl;

  return 0;
}
