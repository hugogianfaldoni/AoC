#include <iostream>
#include <cmath>

bool is_prime(const int n) {
  for (int i = 2; i <= std::sqrt(n); i++) {
    if (n % i == 0)
      return false;
  }
  return true;
}

int main() {
  std::string line;
  std::getline(std::cin, line);

  int b, c, h(0);
  b = std::stoi(line.substr(line.rfind(' ')+1)) * 100 + 100000;
  c = b + 17000;
  
  for ( ; b <= c; b += 17) {
    if (!is_prime(b))
      h++;
  }

  std::cout << h << std::endl;
}
