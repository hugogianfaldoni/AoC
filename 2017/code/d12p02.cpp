#include <iostream>
#include <map>
#include <unordered_set>
#include <vector>
#include <sstream>
#include <queue>

int main() {
  std::unordered_set<int> nodes;
  std::map<int, std::vector<int>> map;
  std::string line;

  while (std::getline(std::cin, line)) {
    std::stringstream ss(line);
    int ID;
    ss >> ID;
    ss.ignore(10000, '>');

    nodes.insert(ID);

    std::string token;
    while (std::getline(ss, token, ',')) {
      int link = std::stoi(token);
      if (link != ID)
        map[ID].push_back(link);
    }
  }

  std::queue<int> to_visit;
  std::unordered_set<int> visited;
  int total_group = 0;

  while (visited.size() != nodes.size()) {
    total_group++;

    // find first node that we never visited
    for (auto const& n: nodes) {
      if (visited.find(n) == visited.end()) {
        to_visit.push(n);
        break;
      }
    }

    while (to_visit.size() > 0) {
      int ID = to_visit.front();
      to_visit.pop();
      visited.insert(ID);

      for (auto const& v: map[ID]) {
        if (visited.find(v) == visited.end()) {
          to_visit.push(v);
        }
      }
    }
  }

  std::cout << total_group << std::endl;

  return 0;
}
