#include <iostream>

int main() {
  std::string input;
  std::getline(std::cin, input);

  size_t length = input.size();
  unsigned int sum1(0), sum2(0);

  for (size_t i = 0; i < length; i++) {
    char c = input[i];
    sum1 += (c == input[(i+1)%length]) ? c - '0' : 0;
    sum2 += (c == input[(i + length/2)%length]) ? c - '0' : 0;
  }

  std::cout << "Part 1: " << sum1 << std::endl;
  std::cout << "Part 2: " << sum2 << std::endl;
}
