#include <iostream>
#include <sstream>
#include <cmath>
#include <numeric>

struct v3 {
  long long x, y, z;
};

v3 get(std::string const& str) {
  v3 v;
  auto p1 = str.find(',');
  auto p2 = str.rfind(',');
  v.x = std::stoi(str.substr(0, p1));
  v.y = std::stoi(str.substr(p1+1, p2 - p1-1));
  v.z = std::stoi(str.substr(p2+1));

  return v;
}

long long vabs(v3 const& v) {
  return std::abs(v.x) + std::abs(v.y) + std::abs(v.z);
}

const long long maxLL = std::numeric_limits<long long>::max();

int main() {
  std::string line;

  v3 min{maxLL, maxLL, maxLL};
  int mini = 0;

  int i = 0;
  while (std::getline(std::cin, line)) {
    if (line.empty()) continue;
    
    std::stringstream ss(line);
    v3 p, v, a;

    auto pos = line.find('<') + 1;
    p = get(line.substr(pos, line.find('>') - pos));
    pos = line.find('<', pos) + 1;
    v = get(line.substr(pos, line.find('>', pos) - pos));
    pos = line.rfind('<') + 1;
    a = get(line.substr(pos, line.rfind('>') - pos));

    auto va(vabs(a)), vv(vabs(v)), vp(vabs(p));
    if (va < min.x) {
      mini = i;
      min = {va, vv, vp};
    }
    else if (va == min.x) {
      if (vv < min.y) {
        mini = i;
        min = {va, vv, vp};
      }
      else if (vv == min.y) {
        if (vp < min.z) {
          mini = i;
          min = {va, vv, vp};
        }
        else {
          std::cout << "error" << std::endl;
        }
      }
    }

    i++;
  }

  std::cout << mini << std::endl;
  return 0;
}
