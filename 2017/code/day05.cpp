#include <iostream>
#include <iterator>
#include <vector>

int main() {
  std::vector<int> array1(std::istream_iterator<int>(std::cin), {});
  std::vector<int> array2 = array1;

  int count = 0;
  for (size_t i = 0; i < array1.size(); count++) {
    i += array1[i]++;
  }
  std::cout << "Part 1: " << count << std::endl;

  count = 0;
  for (size_t i = 0; i < array2.size(); count++) {
    if (array2[i] >= 3)
      i += array2[i]--;
    else 
      i += array2[i]++;
  }
  std::cout << "Part 2: " << count << std::endl;
}
