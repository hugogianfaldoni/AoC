#include <iostream>
#include <unordered_map>
#include <set>
#include <vector>
#include <sstream>
#include <algorithm>

class Solver {
  struct Node {
    int weight;
    int total;
    std::string name;
    std::string parent;
    std::vector<std::string> children;
  };

public:
  std::string const& find_root() {
    std::set<std::string> diff;
    std::set_difference(seen.begin(), seen.end(), children.begin(), children.end(),
        std::inserter(diff, diff.begin()));

    root = *diff.begin();
    return root;
  }

  void parse_input() {
    for (std::string line; std::getline(std::cin, line); ) {
      if (line.empty()) continue;

      std::istringstream iss(line);
      std::string node;
      iss >> node;
      seen.insert(node);

      iss.ignore(10000, '(');
      int weight;
      iss >> weight;

      nodes[node].weight = weight;
      nodes[node].total = weight;
      nodes[node].name = node;

      iss.ignore(10000, '>'); // ignore until we have children nodes
      if (iss.good()) {
        for (std::string token; std::getline(iss, token, ','); ) {
          token.erase(0, 1); // remove first space
          children.insert(token);
          nodes.insert({token, {0,0,token, {}, {}}});
          nodes[token].parent = node;
          nodes[node].children.push_back(token);
        }
      }
    }
  }

  int balance() {
    compute_weight(nodes[root]);
    auto unbalanced = find_unbalanced(nodes[root]);
    
    int diff = 0;
    for (auto const& c: nodes[unbalanced.parent].children) {
      if (c!= unbalanced.name) {
        diff = nodes[c].total- unbalanced.total;
        break;
      }
    }
    return unbalanced.weight + diff;
  }

private:
  void compute_weight(Node& node) {
    if (node.children.empty())
      return;

    for (auto const& c: node.children) {
      compute_weight(nodes[c]);
      node.total += nodes[c].total;
    }
  }

  Node const& find_unbalanced(Node const& node) {
    if (node.children.empty())
      return node;

    int count = 1;
    std::string comp = *node.children.begin();
    auto diff = node.children.end();

    for (auto it = ++node.children.begin(); it != node.children.end(); ++it) {
      if (nodes[comp].total == nodes[*it].total)
        count++;
      else
        diff = it;
    }

    auto unbalanced = ((count == 1) ? node.children.begin() : diff);
    if (unbalanced != node.children.end()) 
      return find_unbalanced(nodes[*unbalanced]);
    return node;
  }

private:
  std::unordered_map<std::string, Node> nodes;
  std::set<std::string> seen, children;
  std::string root;
};

int main() {
  Solver s;
  s.parse_input();
  std::cout << "Part 1: " << s.find_root() << std::endl;
  std::cout << "Part 2: " << s.balance() << std::endl;

  return 0;
}
