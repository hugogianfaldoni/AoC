#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <iterator>

int main() {
  std::vector<int> bank(std::istream_iterator<int>(std::cin), {});
  std::map<std::vector<int>, int> map;

  for (int steps = 0; map.emplace(bank, steps).second; steps++) {
    auto max = std::max_element(bank.begin(), bank.end());
    for (int rep(std::exchange(*max, 0)); rep--; ++*max)
      if (++max == bank.end())
        max = bank.begin();
  }

  std::cout << "Part 1: " << map.size() << std::endl;
  std::cout << "Part 2: " << map.size() - map[bank] << std::endl;
}
