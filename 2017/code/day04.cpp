#include <iostream>
#include <set>
#include <sstream>
#include <algorithm>
#include <iterator>

int main() {
  int res1(0), res2(0);

  for (std::string line; std::getline(std::cin, line); ) {
    std::set<std::string> set;
    std::istringstream iss(line);
    res1 += std::all_of(std::istream_iterator<std::string>(iss), {},
      [&] (auto pass) {
        return set.insert(pass).second;
      });

    iss.clear();
    iss.seekg(0);
    set.clear();

    res2 += std::all_of(std::istream_iterator<std::string>(iss), {},
      [&] (auto pass) {
        std::sort(pass.begin(), pass.end());
        return set.insert(pass).second;
      });
  }
  std::cout << "Part 1: " << res1 << std::endl;
  std::cout << "Part 2: " << res2 << std::endl;
  return 0;
}
