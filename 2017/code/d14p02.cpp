#include <iostream>
#include <vector>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <iomanip>
#include <bitset>
#include <set>
#include <queue>
#include <iterator>

void swap(std::vector<int>& list, size_t a, size_t length) {
  for (int i = 0; i < length/2; i++) {
    int p1 = (a + i)%list.size();
    int p2 = (a+length-1)%list.size() - i;
    if (p2 < 0) p2 += list.size();

    int tmp = list[p1];
    list[p1] = list[p2];
    list[p2] = tmp;
  }
}

std::array<int, 16> hash(std::string const& line) {
  std::vector<int> lengths;

  std::stringstream ss(line);
  char c;

  while (ss.get(c)) {
    lengths.push_back((int)c);
  }

  // add the following to the input (same for everyone)
  lengths.insert(lengths.end(), {17, 31, 73, 47, 23});
  
  std::vector<int> list(256);
  std::iota(list.begin(), list.end(), 0);

  // 64 rounds
  int skip = 0, pos = 0;
  for (size_t r = 0; r < 64; r++) {
    for (size_t i = 0; i < lengths.size(); i++, skip++) {
      int l = lengths[i];
      swap(list, pos, l);
      pos += l + skip;
      pos %= list.size();
    }
  }

  std::array<int, 16> dense{0};
  // build dense hash
  for (size_t i = 0; i < 16; i++) {
    for (size_t j = 0; j < 16; j++) {
      dense[i] ^= list[i*16 + j];
    }
  }
  
  return dense;
}

std::set<std::pair<int, int>> find_group(std::set<std::pair<int, int>> const& grid) {
  std::queue<std::pair<int, int>> to_visit;
  std::set<std::pair<int, int>> visited;

  to_visit.push(*grid.begin());

  auto check_neighbor = [&](int x, int y) -> bool {
    return grid.find({x, y}) != grid.end() && visited.find({x, y}) == visited.end();
  };

  while (!to_visit.empty()) {
    auto p = to_visit.front(); to_visit.pop();
    int x = p.first, y = p.second;
    visited.insert(p);

    if (check_neighbor(x, y+1)) to_visit.push({x, y+1});
    if (check_neighbor(x, y-1)) to_visit.push({x, y-1});
    if (check_neighbor(x+1, y)) to_visit.push({x+1, y});
    if (check_neighbor(x-1, y)) to_visit.push({x-1, y});
  }

  return visited;
}

int main() {
  std::string input;
  std::getline(std::cin, input);

  std::set<std::pair<int, int>> grid;

  for (size_t i = 0; i < 128; i++) {
    auto h = hash(input + "-" + std::to_string(i));
    std::bitset<128> bits;
    for (auto& v: h) {
      bits <<= 8;
      bits |= v;
    }
    
    for (size_t j = 0; j < 128; j++) {
      if (bits[j])
        grid.insert({j, i});
    }
  }

  int groups = 0;
  while (!grid.empty()) {
    auto g = find_group(grid);
    std::set<std::pair<int, int>> diff;
    std::set_difference(grid.begin(), grid.end(), 
        g.begin(), g.end(), std::inserter(diff, diff.begin()));
    grid = diff;
    groups++;
  }

  std::cout << groups << std::endl;

  return 0;
}
