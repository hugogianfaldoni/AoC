#include <iostream>
#include <sstream>
#include <unordered_map>
#include <functional>

class Machine {
  public:
    Machine() {};

    void set_state(char s) {state = s;}
    int get(int pos) {return tape[pos];}
    void set(int pos, int val) {tape[pos] = val;}
    void mleft() {cursor--;}
    void mright() {cursor++;}

    void exec() {
      prog[state]();
    }

    void parse_state() {
      std::string line;
      std::getline(std::cin, line);
      char s = *(line.rbegin()+1);

      std::getline(std::cin, line);
      int cmp1 = std::stoi(&*(line.rbegin()+1));
      
      std::getline(std::cin, line);
      int v1 = std::stoi(&*(line.rbegin()+1));

      std::getline(std::cin, line);
      char d1 = *(line.rbegin()+4);

      std::getline(std::cin, line);
      char ns1 = *(line.rbegin()+1);

      std::getline(std::cin, line);
      int cmp2 = std::stoi(&*(line.rbegin()+1));
      
      std::getline(std::cin, line);
      int v2 = std::stoi(&*(line.rbegin()+1));

      std::getline(std::cin, line);
      char d2 = *(line.rbegin()+4);

      std::getline(std::cin, line);
      char ns2 = *(line.rbegin()+1);

      auto move = [this](char d) {
        if (d == 'l')
          mleft();
        else 
          mright();
      };

      auto f = [=]() {
        if (get(cursor) == cmp1) {
          set(cursor, v1);
          move(d1);
          set_state(ns1);
        }
        else if (get(cursor) == cmp2) {
          set(cursor, v2);
          move(d2);
          set_state(ns2);
        }
      };
      prog[s] = f;
    }

    size_t checksum() {
      size_t count = 0;
      for (auto const& v: tape) {
        if (v.second) count++;
      }
      return count;
    }
  private:
    char state;
    int cursor;
    std::unordered_map<int, int> tape;
    std::unordered_map<char, std::function<void()>> prog;
};

int main() {
  Machine m;

  std::string line, tmp;
  std::getline(std::cin, line);
  m.set_state(*(line.rbegin()+1));

  size_t rep;
  std::getline(std::cin, line);
  std::istringstream ss(line);
  ss >> tmp >> tmp >> tmp >> tmp >> tmp >> rep;
  
  while (std::getline(std::cin, line)) {
    m.parse_state();
  }

  for (size_t i = 0; i < rep; i++) {
    m.exec();
  }

  std::cout << m.checksum() << std::endl;
  
  return 0;
}
