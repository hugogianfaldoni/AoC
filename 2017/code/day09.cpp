#include <iostream>

int main() {
  enum class State {Read, Garbage, Ignore};
  State s(State::Read);

  int groups(0), score(0), garbage_count(0);
  for (char c; std::cin >> c;) {
    switch (s) {
      case State::Read:
        switch (c) {
          case '<': s = State::Garbage; break;
          case '{': groups++; break;
          case '}': score += groups--; break;
        } break;
      case State::Garbage:
        switch (c) {
          case '!': s = State::Ignore; break;
          case '>': s = State::Read; break;
          default: garbage_count++; break;
        } break;
      case State::Ignore:
        s = State::Garbage; // '!' is only present inside garbage
    }
  }

  std::cout << "Part 1: " << score << std::endl;
  std::cout << "Part 2: " << garbage_count << std::endl;
  return 0;
}
