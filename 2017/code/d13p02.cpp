#include <iostream>
#include <sstream>
#include <unordered_map>

int main() {
  std::unordered_map<int, int> firewall;
  
  int size = 0;
  for (std::string line; std::getline(std::cin, line); ) {
    int id = std::stoi(line.substr(0, line.find(':')));
    firewall[id] = std::stoi(line.substr(line.find(':')+1));
    size = std::max(size, id);
  }

  size++;
  bool safe = false;
  int delay = 0;
  while (!safe) {
    safe = true;
    for (int i = 0; i < size; i++) {
      if ((firewall[i] > 0) && ((delay + i) % (firewall[i]*2-2) == 0)) {
        delay++;
        safe = false;
        break;
      }
    }
  }

  std::cout << delay << std::endl;

  return 0;
}
