#include <iostream>
#include <unordered_map>
#include <list>
#include <vector>
#include <functional>
#include <sstream>

using rtype = long long int;
using Register = std::unordered_map<char, long long int>;

using namespace std::placeholders;

struct Env {
  Env() : reg(), sound(), other(nullptr), pc(0), lock(false), finished(false), sent(0) {};
  Register reg;
  std::list<rtype> sound;
  Env* other;
  int pc, sent;
  bool lock, finished;
};

rtype get(Env const& env, std::string const& str) {
  if (str[0] >= 'a' && str[0] <= 'z')
    return env.reg.at(str[0]);
  return std::stoll(str);
}

void set(Env& env, char r1, std::string const& r2) {
  env.reg[r1] = get(env, r2);
}

void add(Env& env, char r1, std::string const& r2) {
  env.reg[r1] += get(env, r2);
}

void mul(Env& env, char r1, std::string const& r2) {
  env.reg[r1] *= get(env, r2);
}

void mod(Env& env, char r1, std::string const& r2) {
  env.reg[r1] %= get(env, r2);
}

void jgz(Env& env, std::string const& r1, std::string const& r2) {
  if (get(env, r1) > 0)
    env.pc += get(env, r2) - 1;
}

void snd(Env& env, std::string const& r1) {
  env.other->sound.push_back(get(env, r1));
  env.sent++;
}

void rcv(Env& env, char r1) {
  if (env.sound.empty()) {
    env.lock = true;
  }
  else {
    env.reg[r1] = env.sound.front();
    env.sound.pop_front();
  }
}

void exec(Env& env, std::vector<std::function<void(Env&)>> const& prog) {
  if (env.lock)
    env.finished = true;

  while (!env.finished) {
    prog[env.pc](env);
    if (env.lock)
      break;
    
    env.pc++;
    if (env.pc < 0 || env.pc >= prog.size())
      env.finished = true;
  }
}

int main() {
  Env env1, env2;
  env1.other = &env2;
  env2.other = &env1;
  std::vector<std::function<void(Env&)>> prog;

  std::string line;
  while (std::getline(std::cin, line)) {
    if (line.empty()) continue;

    std::stringstream ss(line);
    std::string op, r1, r2;
    ss >> op >> r1;
    ss >> r2; // might fail for some op, not important

    if (op == "set") {
      prog.push_back({std::bind(set, _1, r1[0], r2)});
    }
    else if (op == "add") {
      prog.push_back({std::bind(add, _1, r1[0], r2)});
    }
    else if (op == "mul") {
      prog.push_back({std::bind(mul, _1, r1[0], r2)});
    }
    else if (op == "mod") {
      prog.push_back({std::bind(mod, _1, r1[0], r2)});
    }
    else if (op == "jgz") {
      prog.push_back({std::bind(jgz, _1, r1, r2)});
    }
    else if (op == "snd") {
      prog.push_back({std::bind(snd, _1, r1)});
    }
    else if (op == "rcv") {
      prog.push_back({std::bind(rcv, _1, r1[0])});
    }
  }

  env2.reg['p'] = 1;

  while (!env1.finished || !env2.finished) {
    exec(env1, prog);
    if (env2.sound.size() != 0)
      env2.lock = false;

    exec(env2, prog);
    if (env1.sound.size() != 0)
      env1.lock = false;
  }

  std::cout << env2.sent << std::endl;

  return 0;
}
