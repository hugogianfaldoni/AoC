#include <iostream>
#include <vector>
#include <sstream>
#include <numeric>
#include <algorithm>

void swap(std::vector<int>& list, size_t a, size_t length) {
  for (int i = 0; i < length/2; i++) {
    int p1 = (a + i)%list.size();
    int p2 = (a+length-1)%list.size() - i;
    if (p2 < 0) p2 += list.size();

    int tmp = list[p1];
    list[p1] = list[p2];
    list[p2] = tmp;
  }
}

int main() {
  std::vector<int> lengths;

  std::string line;
  std::getline(std::cin, line);
  std::stringstream ss(line);
  int num;
  char tmp;

  while (ss >> num) {
    ss >> tmp;
    lengths.push_back(num);
  }
  
  std::vector<int> list(256);
  std::iota(list.begin(), list.end(), 0);

  for (size_t i = 0, pos = 0; i < lengths.size(); i++) {
    int l = lengths[i];
    swap(list, pos, l);
    pos += l + i;
    pos %= list.size();
  }

  std::cout << list[0] * list[1] << std::endl;
  return 0;
}
