#include <iostream>
#include <vector>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <iomanip>

void swap(std::vector<int>& list, size_t a, size_t length) {
  for (int i = 0; i < length/2; i++) {
    int p1 = (a + i)%list.size();
    int p2 = (a+length-1)%list.size() - i;
    if (p2 < 0) p2 += list.size();

    int tmp = list[p1];
    list[p1] = list[p2];
    list[p2] = tmp;
  }
}

int main() {
  std::vector<int> lengths;

  std::string line;
  std::getline(std::cin, line);
  std::stringstream ss(line);
  char c;

  while (ss.get(c)) {
    lengths.push_back((int)c);
  }

  // add the following to the input (same for everyone)
  lengths.insert(lengths.end(), {17, 31, 73, 47, 23});
  
  std::vector<int> list(256);
  std::iota(list.begin(), list.end(), 0);

  // 64 rounds
  int skip = 0, pos = 0;
  for (size_t r = 0; r < 64; r++) {
    for (size_t i = 0; i < lengths.size(); i++, skip++) {
      int l = lengths[i];
      swap(list, pos, l);
      pos += l + skip;
      pos %= list.size();
    }
  }

  std::vector<int> dense(16, 0);
  // build dense hash
  for (size_t i = 0; i < 16; i++) {
    for (size_t j = 0; j < 16; j++) {
      dense[i] ^= list[i*16 + j];
    }
  }

  for (auto const&v : dense)
    std::cout << std::hex << std::setfill('0') << std::setw(2) << v;
  std::cout << std::endl;

  return 0;
}
