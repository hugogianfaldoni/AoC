#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <iterator>

int main() {
  int sum1(0), sum2(0);

  for (std::string line; std::getline(std::cin, line);) {
    std::istringstream ss(line);
    const std::vector<int> data(std::istream_iterator<int>(ss), {});
    
    // part 1
    auto [min, max] = std::minmax_element(data.begin(), data.end());
    sum1 += *max - *min;

    // part 2
    const size_t size = data.size();
    bool leave = false;
    for (size_t i = 0; i < size && !leave; i++) {
      for (size_t j = i+1; j < size && !leave; j++) {
        const int max = std::max(data[i], data[j]);
        const int min = std::min(data[i], data[j]);
        if (min == 0)
          break;
        if (max % min == 0) {
          sum2 += max / min;
          leave = true;
        }
      }
    }
  }

  std::cout << "Part 1: " << sum1 << std::endl;
  std::cout << "Part 2: " << sum2 << std::endl;
}
