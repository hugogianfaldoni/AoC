#include <iostream>
#include <unordered_set>
#include <array>

template<class T>
struct PairHash {
  size_t operator()(const std::pair<T, T>& p) const {
    std::hash<T> hasher;
    size_t seed = 0;
    seed ^= hasher(p.first) + 0x9e3779b9 + (seed<<6) + (seed>>2);
    seed ^= hasher(p.second) + 0x9e3779b9 + (seed<<6) + (seed>>2);
    return seed;
  }
};

using Infected = std::unordered_set<std::pair<int, int>, PairHash<int>>;

class Grid {
  public:
    struct vec2 {
      int x,y;

      vec2& operator+=(vec2 const& v) {
        x += v.x;
        y += v.y;
        return *this;
      }
    };

    Grid() {
      std::string line;
      size_t i = 0;
      int w = 0;
      while (std::getline(std::cin, line)) {
        if (line.empty()) continue;
        for (size_t j = 0; j < line.size(); j++) {
          if (line[j] == '#')
            infected.emplace(j, i);
        }
        w = line.size();
        i++;
      }

      dir[0] = {0,1};
      dir[1] = {-1,0};
      dir[2] = {0,-1};
      dir[3] = {1,0};

      pos = {w/2, (int)i/2};
      d = 2;
      count = 0;
    }

    void burst() {
      if (infected.find({pos.x, pos.y}) != infected.end()) {
        d++;
        if(d > 3) d = 0;
        infected.erase({pos.x, pos.y});
      }
      else {
        d--;
        if (d < 0) d = 3;
        infected.emplace(pos.x, pos.y);
        count++;
      }
      pos += dir[d];
    }

    int infected_count() const {return count;};

  private:
    Infected infected;
    vec2 pos;
    int d;
    std::array<vec2, 4> dir;
    int count;
};

int main() {
  Grid grid;

  for (size_t i = 0; i < 10000; i++)
    grid.burst();

  std::cout << grid.infected_count() << std::endl;
  return 0;
}
