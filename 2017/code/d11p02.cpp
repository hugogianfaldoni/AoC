#include <iostream>
#include <sstream>
#include <map>
#include <cmath>

int distance(std::map<std::string, int>& map) {
  int x = map["ne"] + map["se"] - map["nw"] - map["sw"];
  int y = map["n"] + map["nw"] - map["s"] - map["se"];
  int z = map["s"] + map["sw"] - map["n"] - map["ne"];

  return (std::abs(x) + std::abs(y) + std::abs(z))/2;
}

int main() {
  std::map<std::string, int> map;

  std::string input, token;
  std::getline(std::cin, input);
  std::stringstream ss(input);

  int max = 0;
  while (std::getline(ss, token, ',')) {
    map[token]++;
    max = std::max(max, distance(map));
  }

  std::cout << max << std::endl;
  return 0;
}
