#include <iostream>
#include <unordered_map>
#include <any>
#include <sstream>
#include <functional>

using rtype = long long int;
using Register = std::unordered_map<char, rtype>;
using namespace std::placeholders;

class Prog {
  public:
    Prog(): pc(0), count(0) {

    }

    // read programme from std::cin
    void init() {
      std::string line;
      while (std::getline(std::cin, line)) {
        if (line.empty()) continue;
        std::stringstream ss(line);
        std::string op, r1, r2;
        ss >> op >> r1 >> r2;
        if (op == "set") {
          prog.push_back({std::bind(&Prog::set, this, r1[0], any_from_str(r2))});
        }
        else if (op == "sub") {
          prog.push_back({std::bind(&Prog::sub, this, r1[0], any_from_str(r2))});
        }
        else if (op == "mul") {
          prog.push_back({std::bind(&Prog::mul, this, r1[0], any_from_str(r2))});
        }
        else if (op == "jnz") {
          prog.push_back({std::bind(&Prog::jnz, this, any_from_str(r1), any_from_str(r2))});
        }
      }
    }

    void run() {
      bool finished = false;
      while (!finished) {
        prog[pc]();
        pc++;
        if (pc < 0 || pc >= prog.size())
          finished = true;
      }
    }

    void print_count() const {
      std::cout << count << std::endl;
    }

  private:
    void set(char r1, std::any const& r2) {
      reg[r1] = get(r2);
    }
    void sub(char r1, std::any const& r2) {
      reg[r1] -= get(r2);
    }
    void mul(char r1, std::any const& r2) {
      reg[r1] *= get(r2);
      count++;
    }
    void jnz(std::any const& r1, std::any const& r2) {
      if (get(r1) != 0) {
        pc += get(r2) - 1;
      }
    }

    bool is_reg(std::any const& val) const {
      return val.type() == typeid(char);
    }
    rtype get(std::any const& val) {
      if (is_reg(val)) 
        return reg[std::any_cast<char>(val)];
      return std::any_cast<rtype>(val);
    }
    static std::any any_from_str(std::string const& str) {
      if (str[0] >= 'a' && str[0] <= 'z')
        return {str[0]};
      return {std::stoll(str)};
    }

  private:
    Register reg;
    int pc;
    std::vector<std::function<void()>> prog;
    int count;
};

int main () {
  Prog prog;

  prog.init();
  prog.run();
  prog.print_count();

  return 0;
}
