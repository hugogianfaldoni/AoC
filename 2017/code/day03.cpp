#include <iostream>
#include <array>
#include <cmath>

class Grid {
  struct Pos {
    int x, y;
  };

  public:
  Grid(): grid({0}), pos({64, 64}), shift(1), current_shift(0), count(0), dir(0) {
    grid[pos.x][pos.y] = 1;
  }

    int solve(int input) {
      bool found = false;
      int sum = 0;
      while (!found) {
        next_pos();
        sum = sum_neighbors(pos.x, pos.y);
        if (sum > input)
          found = true;
        grid[pos.x][pos.y] = sum;
      }
      return sum;
    }
  private:
    int sum_neighbors(int x, int y) const{
      return
        grid[x-1][y] +
        grid[x-1][y-1] +
        grid[x-1][y+1] +
        grid[x][y-1] +
        grid[x][y+1] +
        grid[x+1][y] +
        grid[x+1][y-1] +
        grid[x+1][y+1];
    }

    void next_pos() {
      if (count == 2) {
        shift++;
        count = 0;
      }

      switch(dir) {
        case 0:
          pos.x++;
          break;
        case 1:
          pos.y++;
          break;
        case 2:
          pos.x--;
          break;
        case 3:
          pos.y--;
      }

      current_shift++;
      if (current_shift == shift) {
        current_shift = 0;
        count++;
        dir++;
        dir %= 4;
      }
    }

  private:
    std::array<std::array<int, 128>, 128> grid;
    Pos pos;

    int shift;
    int current_shift;
    int count;
    int dir;
};

int main() {
  int addr;
  std::cin >> addr;

  // corner
  int corner = std::ceil(std::sqrt(addr));
  corner += (corner + 1) % 2;
  int layer = (corner - 1) / 2;
  corner *= corner;

  int middle = corner - layer;
  int dist = 
    std::min(
      std::min(std::abs(middle - addr), std::abs(middle - 2*layer - addr)),
      std::min(std::abs(middle - 4*layer - addr), std::abs(middle - 6*layer - addr))) + layer;

  std::cout << "Part 1: " << dist << std::endl;

  Grid grid;
  std::cout << "Part 2: " << grid.solve(addr) << std::endl;
}
