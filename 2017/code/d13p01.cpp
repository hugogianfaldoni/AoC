#include <iostream>
#include <sstream>
#include <map>

int main() {
  std::map<int, int> firewall;
  
  int severity = 0;

  for (std::string line; std::getline(std::cin, line); ) {
    int id = std::stoi(line.substr(0, line.find(':')));
    firewall[id] = std::stoi(line.substr(line.find(':')+1));
  }

  int size = firewall.rbegin()->first;
  for (int i = 0; i < size; i++) {
    int scanner = -1;
    if (firewall[i] > 0)
      scanner = i % (firewall[i]*2-2);
    if (scanner == 0)
      severity += firewall[i]*i;
  }

  std::cout << severity << std::endl;

  return 0;
}
