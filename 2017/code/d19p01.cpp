#include <iostream>
#include <vector>
#include <algorithm>
#include <cctype>

struct Pos {
  int x, y;
};

// dir: 0=N, 1=E, 2=S, 3=W
std::string walk(std::vector<std::string> const& grid, Pos pos, int dir) {
  if (pos.x < 0 || pos.x >= grid[0].size() || pos.y < 0 || pos.y >= grid.size())
    return {};

  char cur = grid[pos.y][pos.x];

  int x = pos.x, y = pos.y;
  Pos neighbor[4] = {
    {x, y-1},
    {x+1, y},
    {x, y+1},
    {x-1, y}
  };

  Pos next;
  if (cur == '+') {
    for (size_t i = 0; i < 4; i++) {
      if (i == std::abs(dir-2))
        continue;
      if (grid[neighbor[i].y][neighbor[i].x] != ' ') {
        next = neighbor[i];
        dir = i;
        break;
      }
    }
    return walk(grid, next, dir);
  }
  else if (std::isalpha(cur)) {
    return cur + walk(grid, neighbor[dir], dir);
  }
  else {
    return walk(grid, neighbor[dir], dir);
  }
}

int main() {
  std::vector<std::string> grid;

  std::string line;
  while (std::getline(std::cin, line))
    grid.push_back(line);

  std::cout << walk(grid, {(int)grid[0].find('|'), 0}, 2) << std::endl;
  return 0;
}
