#include <iostream>

int main() {
  long seedA, seedB;

  std::cin >> seedA >> seedB;

  // same for everyone
  const long factorA = 16807, factorB = 48271;
  const long divider = 2147483647;

  int count = 0;
  for (size_t i = 0; i < 5000000; i++) {
    do {
      seedA = (seedA*factorA)%divider;
    } while (seedA % 4 != 0);

    do {
      seedB = (seedB*factorB)%divider;
    } while (seedB % 8 != 0);

    if ((seedA & 0xffff) == (seedB & 0xffff)) {
      count++;
    }
  }
  
  std::cout << count << std::endl;

  return 0;
}
