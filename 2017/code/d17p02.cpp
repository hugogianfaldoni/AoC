#include <iostream>

int main() {
  int step;
  std::cin >> step;
  step++;

  const size_t max(50000000);
  size_t i(1), pos(0), n, saved;

  while (i <= max) {
    pos += step;
    if (pos >= i)
      pos %= i;

    if (pos == 0)
      saved = i;

    n = (i - pos) / step;
    pos += n * step;
    i += n + 1;
  }

  std::cout << saved << std::endl;
  return 0;
}
