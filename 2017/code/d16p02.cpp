#include <iostream>
#include <sstream>
#include <vector>
#include <array>
#include <numeric>
#include <algorithm>
#include <functional>

using namespace std::placeholders;

void shift(std::string& dance, int val) {
  std::rotate(dance.rbegin(), dance.rbegin()+val, dance.rend());
}

void exchange(std::string& dance, int a, int b) {
  std::swap(dance[a], dance[b]);
}

void partner(std::string& dance, char c1, char c2) {
  std::swap(dance[dance.find(c1)], dance[dance.find(c2)]);
}

int main() {
  std::string input, token;
  std::getline(std::cin, input);
  std::stringstream ss(input);

  std::string dance{"abcdefghijklmnop"};

  std::vector<std::function<void(std::string&)>> call;

  while (std::getline(ss, token, ',')) {
    if (token.empty()) continue;
    switch (token[0]) {
      case 's': {
        int s = std::stoi(token.substr(1))%16;
        call.push_back({std::bind(shift, _1, s)});
      } break;
      case 'x': {
        auto pos = token.find('/');
        int a = std::stoi(token.substr(1, token.size()-pos));
        int b = std::stoi(token.substr(pos+1));
        call.push_back({std::bind(exchange, _1, a, b)});
      } break;
      case 'p': {
        call.push_back({std::bind(partner, _1, token[1], token[3])});
      } break;
      default: break;
    }
  }

  // find cycle
  auto copy = dance;
  size_t cycle = 0;
  do {
    for (auto const& c: call)
      c(copy);
    cycle++;
  } while (copy != dance);

  std::cout << "Cycle: " << cycle << std::endl;

  const size_t max = 1000000000 % cycle;
  for (size_t i = 0; i < max; i++) {
    for (auto const& c: call)
      c(dance);
  }

  std::cout << dance << std::endl;

  return 0;
}
