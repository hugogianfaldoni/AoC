#include <iostream>
#include <functional>
#include <unordered_map>
#include <sstream>

using Register = std::unordered_map<std::string, int>;

int main() {
  static std::unordered_map<std::string, 
    std::function<bool(Register& reg, std::string const& r, int val)>> func{
      {"<", [](Register& reg, std::string const& r, int val) {return reg[r] < val;}},
      {">", [](Register& reg, std::string const& r, int val) {return reg[r] > val;}},
      {"==", [](Register& reg, std::string const& r, int val) {return reg[r] == val;}},
      {"!=", [](Register& reg, std::string const& r, int val) {return reg[r] != val;}},
      {"<=", [](Register& reg, std::string const& r, int val) {return reg[r] <= val;}},
      {">=", [](Register& reg, std::string const& r, int val) {return reg[r] >= val;}}
    };
  Register reg;
  int max(0);
  for (std::string line; std::getline(std::cin, line); ) {
    if (line.empty()) continue;

    std::istringstream iss(line);
    int val, cval;
    std::string r1, r2, op, cmp, tmp;
    iss >> r1 >> op >> val >> tmp >> r2 >> cmp >> cval;
    if (func[cmp](reg, r2, cval)) {
      if (op == "inc")
        reg[r1] += val;
      else
        reg[r1] -= val;
      max = std::max(max, reg[r1]);
    }
  }

  std::cout << "Part 1: " <<
    std::max_element(reg.begin(), reg.end(), [] (auto const& p1, auto const& p2) {
        return p1.second < p2.second;
        })->second << std::endl;
  std::cout << "Part 2: " << max << std::endl;
}
