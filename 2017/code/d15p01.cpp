#include <iostream>

int main() {
  long seedA, seedB;

  std::cin >> seedA >> seedB;

  // same for everyone
  const long factorA = 16807, factorB = 48271;
  const long divider = 2147483647;

  int count = 0;
  for (size_t i = 0; i < 40000000; i++) {
    seedA = (seedA*factorA)%divider;
    seedB = (seedB*factorB)%divider;

    if ((seedA & 0xffff) == (seedB & 0xffff)) {
      count++;
    }
  }
  
  std::cout << count << std::endl;

  return 0;
}
