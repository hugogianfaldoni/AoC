#include <iostream>
#include <unordered_map>
#include <array>

template<class T>
struct PairHash {
  size_t operator()(const std::pair<T, T>& p) const {
    std::hash<T> hasher;
    size_t seed = 0;
    seed ^= hasher(p.first) + 0x9e3779b9 + (seed<<6) + (seed>>2);
    seed ^= hasher(p.second) + 0x9e3779b9 + (seed<<6) + (seed>>2);
    return seed;
  }
};

using Infected = std::unordered_map<std::pair<int, int>, int, PairHash<int>>;

class Grid {
  public:
    struct vec2 {
      int x,y;

      vec2& operator+=(vec2 const& v) {
        x += v.x;
        y += v.y;
        return *this;
      }
    };

    Grid() {
      std::string line;
      int i = 0;
      int w = 0;
      while (std::getline(std::cin, line)) {
        if (line.empty()) continue;
        for (int j = 0; j < line.size(); j++) {
          if (line[j] == '#')
            infected[{j, i}] = 2;
        }
        w = line.size();
        i++;
      }

      dir[0] = {0,1};
      dir[1] = {-1,0};
      dir[2] = {0,-1};
      dir[3] = {1,0};

      pos = {w/2, i/2};
      d = 2;
      count = 0;
    }

    void burst() {
      int& s = infected[{pos.x, pos.y}];
      if (s == 0) { // clean
        s = 1;
        d--;
      }
      else if (s == 1) { // weakened
        s = 2;
        count++;
      }
      else if (s == 2) { // infected
        s = 3;
        d++;
      }
      else { // flagged
        s = 0;
        d += 2;
      }
      d += 4;
      d %= 4;
      pos += dir[d];
    }

    int infected_count() const {return count;};

  private:
    Infected infected;
    vec2 pos;
    int d;
    std::array<vec2, 4> dir;
    int count;
};

int main() {
  Grid grid;

  for (size_t i = 0; i < 10000000; i++)
    grid.burst();

  std::cout << grid.infected_count() << std::endl;
  return 0;
}
