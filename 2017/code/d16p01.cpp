#include <iostream>
#include <sstream>
#include <vector>
#include <numeric>
#include <algorithm>

int main() {
  std::string input, token;
  std::getline(std::cin, input);
  std::stringstream ss(input);

  std::vector<char> dance(16);
  std::iota(dance.begin(), dance.end(), 'a');

  auto print = [&]() {
    for (auto& c: dance)
      std::cout << c;
    std::cout << std::endl;
  };

  while (std::getline(ss, token, ',')) {
    if (token.empty()) continue;
    switch (token[0]) {
      case 's': {
        int shift = std::stoi(token.substr(1))%16;
        for (int i = 0; i < shift; i++) {
          char c = dance.back(); dance.pop_back();
          dance.insert(dance.begin(), c);
        }
      } break;
      case 'x': {
        auto pos = token.find('/');
        int a = std::stoi(token.substr(1, token.size()-pos));
        int b = std::stoi(token.substr(pos+1));
        int tmp = dance[a];
        dance[a] = dance[b];
        dance[b] = tmp;
      } break;
      case 'p': {
        auto p1 = std::find(std::begin(dance), std::end(dance), token[1]);
        auto p2 = std::find(std::begin(dance), std::end(dance), token[3]);
        std::iter_swap(p1, p2);
      } break;
      default: break;
    }
  }

  print();

  return 0;
}
