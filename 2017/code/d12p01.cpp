#include <iostream>
#include <map>
#include <unordered_set>
#include <vector>
#include <sstream>
#include <queue>

int main() {
  std::map<int, std::vector<int>> map;
  std::string line;

  while (std::getline(std::cin, line)) {
    std::stringstream ss(line);
    int ID;
    ss >> ID;
    ss.ignore(10000, '>');

    std::string token;
    while (std::getline(ss, token, ',')) {
      int link = std::stoi(token);
      if (link != ID)
        map[ID].push_back(link);
    }
  }

  std::queue<int> to_visit;
  std::unordered_set<int> visited;
  to_visit.push(0);

  while (to_visit.size() > 0) {
    int ID = to_visit.front();
    to_visit.pop();
    visited.insert(ID);

    for (auto const& v: map[ID]) {
      if (visited.find(v) == visited.end()) {
        to_visit.push(v);
      }
    }
  }

  std::cout << visited.size() << std::endl;

  return 0;
}
