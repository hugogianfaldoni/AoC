#include <iostream>
#include <vector>
#include <algorithm>

struct Piece {
  Piece(): a(0), b(0), used(false) {}
  Piece(int a, int b, bool used): a(a), b(b), used(used) {}

  int a, b;
  bool used;
};

class Solver {
  public:
    Solver(): max_strength(0), max_length_strength(0, 0) {}

    void parse_input() {
      std::string line;
      while (std::getline(std::cin, line)) {
        auto pos = line.find('/');
        const int a = std::stoi(line.substr(0, pos));
        const int b = std::stoi(line.substr(pos+1));
        pieces.emplace_back(a, b, false);
      }
    }

    void solve(int port = 0, int length = 0, int strength = 0) {
      max_strength = std::max(strength, max_strength);
      max_length_strength.first = std::max(length, max_length_strength.first);

      if (max_length_strength.first == length)
        max_length_strength.second = strength;

      for (auto& p: pieces) {
        if (!p.used && (p.a == port || p.b == port)) {
          p.used = true;
          const int nport = (p.a == port) ? p.b : p.a;
          solve(nport, length + 1, strength + p.a + p.b);
          p.used = false;
        }
      }
    }

    void print_result() {
      std::cout << "Part 1: " << max_strength << std::endl;
      std::cout << "Part 2: " << max_length_strength.second << std::endl;
    }
  private:
    int max_strength;
    std::pair<int, int> max_length_strength;
    std::vector<Piece> pieces;
};
int main() {
  Solver s;

  s.parse_input();
  s.solve();
  s.print_result();

  return 0;
}
