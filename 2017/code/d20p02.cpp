#include <iostream>
#include <sstream>
#include <cmath>
#include <numeric>
#include <map>

using vec = std::tuple<long long, long long, long long>;

struct v3 {
  long long x, y, z;

  vec get() const {return {x, y, z};};
};

v3 get(std::string const& str) {
  v3 v;
  auto p1 = str.find(',');
  auto p2 = str.rfind(',');
  v.x = std::stoi(str.substr(0, p1));
  v.y = std::stoi(str.substr(p1+1, p2 - p1-1));
  v.z = std::stoi(str.substr(p2+1));

  return v;
}

long long vabs(v3 const& v) {
  return std::abs(v.x) + std::abs(v.y) + std::abs(v.z);
}

const long long maxLL = std::numeric_limits<long long>::max();

int main() {
  std::string line;
  std::multimap<vec, std::pair<vec, vec>> particles;

  while (std::getline(std::cin, line)) {
    if (line.empty()) continue;
    
    std::stringstream ss(line);
    v3 p, v, a;

    auto pos = line.find('<') + 1;
    p = get(line.substr(pos, line.find('>') - pos));
    pos = line.find('<', pos) + 1;
    v = get(line.substr(pos, line.find('>', pos) - pos));
    pos = line.rfind('<') + 1;
    a = get(line.substr(pos, line.rfind('>') - pos));

    particles.emplace(p.get(), std::make_pair(v.get(), a.get()));
  }

  std::multimap<vec, std::pair<vec, vec>> new_particles = particles;
  particles.clear();

  // we need i because some steps don't generate collisions but we get some afterwards
  for (int i = 0; i < 100;) {
    particles = new_particles;
    new_particles.clear();

    // check collisions
    for (auto& P: particles) {
      vec p = P.first;
      vec v = P.second.first;
      vec a = P.second.second;

      long long px, py, pz, vx, vy, vz, ax, ay, az;
      std::tie(px, py, pz) = p;
      std::tie(vx, vy, vz) = v;
      std::tie(ax, ay, az) = a;

      vx += ax; vy += ay; vz += az;
      px += vx; py += vy; pz += vz;

      new_particles.emplace(std::make_tuple(px, py, pz), 
          std::make_pair(std::make_tuple(vx, vy, vz), a));
    }

    // remove them
    for (auto it = new_particles.begin(); it != new_particles.end();) {
      if (new_particles.count(it->first) > 1) {
        new_particles.erase(it->first);
        // need to restart from the beginning
        it = new_particles.begin();
      }
      else {
        ++it;
      }
    }

    // hacky hack to get all collisions
    if (new_particles.size() == particles.size())
      i++;
    else 
      i = 0;
  }

  std::cout << particles.size() << std::endl;
  return 0;
}
