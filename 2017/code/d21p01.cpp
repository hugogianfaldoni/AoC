#include <iostream>
#include <sstream>
#include <vector>
#include <array>
#include <algorithm>
#include <unordered_map>
#include <cmath>

class Matrix {
  public:
    Matrix() : data() {}

    Matrix(std::string const& str) {
      std::istringstream ss(str);
      std::string token;
      while (std::getline(ss, token, '/'))
        data.push_back(token);
    }

    Matrix(std::vector<Matrix> const& mv) {
      if (mv.empty()) {
        Matrix();
        return;
      }

      const size_t count = std::sqrt(mv.size()); // sub matrix count on one axis
      const size_t ssize = mv[0].size(); // sub matrix size
      const size_t size = ssize*count; // size of the new matrix

      //std::cout << "size: " << size << std::endl;
      //std::cout << "ssize: " << ssize << std::endl;

      data = std::vector<std::string>{size};
      for (auto& s: data)
        s = std::string(size, ' ');

      for (size_t k = 0; k < mv.size(); k++) {
        for (size_t row = 0; row < ssize; row++) {
          for (size_t col = 0; col < ssize; col++) {
            size_t x = col + (k/count)*ssize;
            size_t y = row + (k%count)*ssize;
            //std::cout << "k=" << k << " (" << x << ";" << y << ")\n";
            data[y][x] = mv[k][row][col];
          }
        }
      }
    }
    
    size_t size() const {
      return data.size();
    }

    size_t count(char c) const {
      size_t ret = 0;
      for (auto const& l: data) {
        ret += std::count_if(l.begin(), l.end(), [c](char _c) {return c == _c;});
      }

      return ret;
    }

    std::string to_str() const {
      if (data.empty()) return {};
      std::string ret;
      for (auto const& l: data)
        ret += l + '/';
      ret.pop_back(); // remove last /
      return ret;
    }

    Matrix& fliph() {
      for (auto& l: data)
        std::reverse(l.begin(), l.end());
      return *this;
    }

    Matrix& transpose() {
      for (size_t col = 0; col < data.size() - 1; col++) {
        for (size_t row = col+1; row < data.size(); row++) {
          std::swap(data[col][row], data[row][col]);
        }
      }
      return *this;
    }

    std::vector<Matrix> split() {
      size_t size = data.size();
      size_t nsize = (data.size() % 2 == 0) ? 2 : 3;
      std::vector<std::string> str((size/nsize)*(size/nsize));

      size_t cur = 0;
      for (size_t col = 0; col < size; col += nsize) {
        for (size_t row = 0; row < size; row++) {
          size_t x = col/nsize;
          size_t y = row/nsize;
          cur = x*(size/nsize) + y;
          for (size_t i = 0; i < nsize; i++) {
            str[cur] += data[row][col+i];
          }
          str[cur] += '/';
        }
      }

      std::vector<Matrix> ret;
      for (auto& s: str) {
        s.pop_back();
        ret.emplace_back(s);
      }

      return ret;
    }

    // rotate right by 90 degrees
    Matrix& rotate(bool left = false) {
      transpose();
      if (left)
        fliph();
      else 
        std::reverse(data.begin(), data.end());
      return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, Matrix const& m) {
      for (auto const& l: m.data)
        os << l << "\n";
      return os;
    }

    std::string& operator[](size_t pos) {
      return data[pos];
    }

    std::string const& operator[](size_t pos) const {
      return data[pos];
    }

  private:
    std::vector<std::string> data;
};

std::array<std::string, 8> build_transforms(std::string const& t) {
  std::array<std::string, 8> ret;
  
  Matrix m(t);
  
  ret[0] = t;
  ret[1] = m.rotate().to_str();
  ret[2] = m.rotate().to_str();
  ret[3] = m.rotate().to_str();
  
  m.rotate();

  ret[4] = m.fliph().to_str();
  ret[5] = m.rotate().to_str();
  ret[6] = m.rotate().to_str();
  ret[7] = m.rotate().to_str();

  return ret;
}

int main() {
  std::unordered_map<std::string, std::string> transform;

  std::string line;
  while (std::getline(std::cin, line)) {
    if (line.empty()) continue;
    std::string p(line.substr(0, line.find(' ')));
    std::string t(line.substr(line.rfind(' ')+1));
    for (auto const& a: build_transforms(p))
      transform[a] = t;
  }

  Matrix m(".#./..#/###");

  for (size_t i = 0; i < 5; i++) {
    auto mv = m.split();
    for (size_t i = 0; i < mv.size(); i++) {
      mv[i] = transform[mv[i].to_str()];
    }
    m = Matrix(mv);
  }
 
  std::cout << m.count('#') << std::endl;
  return 0;
}
