#include <iostream>
#include <vector>
#include <algorithm>

#include "utils.hpp"

int main() {
	std::string input;
	std::getline(std::cin, input);

	auto build_stack = [](std::string const& str, char ign = 0) {
		std::vector<char> stack;
		for (auto const& c: str) {
			if ((c | 0x20) == ign)
				continue;
			if (stack.empty()) {
				stack.push_back(c);
			}
			else if (char a = stack.back(); (a | 0x20) == (c | 0x20) && a != c) {
				stack.pop_back();
			}
			else {
				stack.push_back(c);
			}
		}

		return stack.size();
	};

	std::cout << build_stack(input) << std::endl;

	auto min = input.size();
	for (char i = 'a'; i <= 'z'; i++) {
		auto size = build_stack(input, i);
		if (size < min)
			min = size;
	}

	std::cout << min << std::endl;

	return 0;
}
