#include <iostream>
#include <vector>
#include <unordered_set>

int main() {
	std::unordered_set<int> s;
	std::vector<int> nums;
	int sum1 = 0, sum = 0, rep = 0;

	for (std::string line; std::getline(std::cin, line);)
		nums.emplace_back(std::stoi(line));

	bool first = true;
	bool found = false;
	do {
		for (auto const& n: nums) {
			sum += n;
			if (!s.insert(sum).second) {
				found = true;
				rep  = sum;
				break;
			}
		}
		if (first) {
			sum1 = sum;
			first = false;
		}
	} while (!found);

	std::cout << "Part 1: " << sum1 << std::endl;
	std::cout << "Part 2: " << rep << std::endl;

	return 0;
}
