#ifndef DEF_UTILS_HPP
#define DEF_UTILS_HPP

#include <algorithm>
#include <functional>
#include <string>
#include <vector>

namespace {
	namespace util {

		static std::string alpha{"abcdefghijklmnopqrstuvwxyz"};
		static std::string ALPHA{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
		static std::string num{"1234567890"};
		static std::string hex{"1234567890abdcdef"};
		static std::string HEX{"1234567890ABCDEF"};

		/*
		 * Remove any chars found in @chars from the beginning and the end of @str
		 */
		std::string trim(std::string const& str, std::string const& chars) {
			auto const beg = str.find_first_not_of(chars);
			if (beg == std::string::npos)
				return {};

			auto const end = str.find_last_not_of(chars);

			return str.substr(beg, end - beg + 1);
		}

		/*
		 * trim the string @str from any chars found in @chars
		 * replace any consecutive @chars in @str with one @fill
		 */
		std::string reduce(std::string const& str, std::string const& chars, std::string const& fill) {
			auto res = trim(str, chars);

			auto beg = res.find_first_of(chars);
			while (beg != std::string::npos) {
				auto const end = res.find_first_not_of(chars, beg);

				res.replace(beg, end - beg, fill);

				auto const new_beg = beg + fill.size();
				beg = res.find_first_of(chars, new_beg);
			}

			return res;
		}

		/*
		 * Splits a string using @delimiter
		 * if keep_empty is true, split("|aaaa||bbbb|", "|", true) will result in ["", "aaaa", "", "bbbb", ""]
		 * else it will be ["aaaa", "bbbb"]
		 *
		 * if beg_end_empty is true , split("|aaaa||bbbb|", "|", false, true) will result in ["", "aaaa", "bbbb", ""]
		 * else it will be ["aaaa", "bbbb"]
		 *
		 * if keep_empty = false and beg_end_empty = true, the first and last element of the returned array can be empty
		 * but no element in the middle will be empty.
		 *
		 * lastely, split("aaaa", "|", ---, ---) = []
		 */
		template<typename Out>
			void split(std::string const& str, std::string const& delimiter, Out result, bool keep_empty = false, bool beg_end_empty = false) {
				std::string::size_type beg = 0;
				std::string::size_type end = 0;

				while (1) {
					end = str.find(delimiter, beg);
					std::string const token = str.substr(beg, end - beg);
					bool keep = true;
					if (token.empty()) {
						keep = false;

						bool beg_or_end = (beg == 0 || end == std::string::npos);
						if ((!beg_or_end && keep_empty) || (beg_or_end && beg_end_empty))
							keep = true;
					}

					if (keep)
						*(result++) = token;
					if (end == std::string::npos)
						break;
					beg = end + delimiter.size();
				}
			}

		/*
		 * Vector version of split for convenience
		 */
		std::vector<std::string> split(std::string const& str, std::string const& delimiter, bool keep_empty = false, bool beg_end_empty = false) {
			std::vector<std::string> ret;
			split(str, delimiter, std::back_inserter(ret), keep_empty, beg_end_empty);
			return ret;
		}

		std::vector<std::string> split_not(std::string const& str, std::string const& not_str) {
			std::vector<std::string> ret;

			std::string cur;
			for (char c: str) {
				if (not_str.find(c) != std::string::npos) {
					cur += c;
				}
				else if (!cur.empty()) {
					ret.push_back(cur);
					cur.clear();
				}
			}

			if (!cur.empty())
				ret.push_back(cur);

			return ret;
		}

		template <typename F, typename T = typename std::result_of<F(std::string const&)>::type>
		std::vector<T> split_not(std::string const& str, std::string const& not_str, F const& f) {
			std::vector<T> ret;
			for (auto const& v: split_not(str, not_str))
				ret.emplace_back(f(v));
			return ret;
		}

		int str_int(std::string const& str) {
			return std::stoi(str);
		}

		void hash_combine(std::size_t& h, std::size_t const& v) {
			h ^= v + 0x9e3779b9 + (h << 6) + (h >> 2);
		}

		/*
		 * Hash a pair<T, T>
		 */
		struct pair_hash {
			template <typename T, typename U>
			std::size_t operator()(std::pair<T, U> const& p) const {
				std::size_t h = std::hash<T>()(p.first);
				hash_combine(h, std::hash<U>()(p.second));
				return h;
			}
		};
	}
}

#endif
