#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>

int main() {
	std::vector<std::string> lines;

	int threes = 0, twos = 0;
	for (std::string line; std::getline(std::cin, line);) {
		lines.push_back(line);

		std::unordered_map<char, int> count;
		for (auto c: line)
			count[c]++;

		if (std::any_of(count.begin(), count.end(), [](auto const& p){return p.second == 3;}))
			threes++;
		if (std::any_of(count.begin(), count.end(), [](auto const& p){return p.second == 2;}))
			twos++;
	}

	std::cout << threes * twos << std::endl;

	std::size_t len = lines[0].size();
	for (int i = 0; i < len; i++) {
		std::unordered_set<std::string> set;
		for (auto const&l: lines) {
			std::string str = l.substr(0, i) + l.substr(i+1, len - i - 1);
			if (!set.insert(str).second) {
				std::cout << str << std::endl;
				return 0;
			}
		}
	}

	return 0;
}
