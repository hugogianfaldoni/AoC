#include <iostream>
#include <tuple>
#include <map>
#include <unordered_map>

#include "utils.hpp"

int main() {

	std::vector<std::string> input;
	for (std::string line; std::getline(std::cin, line);) {
		if (line.empty()) continue;
		input.push_back(line);
	}

	std::sort(input.begin(), input.end());

	std::unordered_map<int, std::vector<std::pair<int, int>>> guard;
	int cur_id = -1;
	int sleep = 0, wake = 0;
	for (auto const& line: input) {
		auto vec = util::split(line, "]");
		auto date_vec = util::split_not(vec[0], util::num, &util::str_int);

		// int year = date_vec[0];
		// int month = date_vec[1];
		// int day = date_vec[2];
		// int h = date_vec[3];
		int m = date_vec[4];

		auto id_vec = util::split_not(vec[1], util::num, &util::str_int);
		if (!id_vec.empty()) {
			cur_id = id_vec[0];
		}
		else if (vec[1].find("fall") != std::string::npos) {
			sleep = m;
		}
		else { // wake up
			wake = m;
			guard[cur_id].emplace_back(sleep, wake);
		}
	}

	int max = 0, max_id = 0;
	std::unordered_map<int, std::unordered_map<int, int>> gsleep;
	for (auto const& g: guard) {
		auto id = g.first;
		int sleep = 0;

		for (auto const& [s, w]: g.second) {
			for (int i = s; i < w; i++) {
				gsleep[id][i]++;
			}
			sleep += w - s;
		}
		if (sleep > max) {
			max = sleep;
			max_id = id;
		}
	}

	int mmax = 0;
	int mmax_val = 0;
	std::unordered_map<int, int> sleep_count;
	for (auto const& [s, w]: guard[max_id]) {
		for (int i = s; i < w; i++) {
			sleep_count[i]++;
			if (sleep_count[i] > mmax_val) {
				mmax_val = sleep_count[i];
				mmax = i;
			}
		}
	}

	std::cout << max_id * mmax << std::endl;

	max = 0;
	mmax = 0;
	max_id = 0;
	for (auto const& [id, sc]: gsleep) {
		for (auto const& [m, c]: gsleep[id]) {
			if (c > max) {
				max_id = id;
				max = c;
				mmax = m;
			}
		}
	}

	std::cout << max_id * mmax << std::endl;

	return 0;
}
