#include <iostream>
#include <vector>

#include "utils.hpp"

struct rect {
	int id, x, y, w, h;
};

int main() {
	std::vector<rect> input;
	int grid[1000][1000] = {0};
	int total = 0;

	for (std::string line; std::getline(std::cin, line);) {
		if (line.empty()) continue;
		auto vec = util::split_not(line, util::num, [](std::string const&s){return std::stoi(s);});

		int id = vec[0];
		int x  = vec[1];
		int y  = vec[2];
		int w  = vec[3];
		int h  = vec[4];
		input.push_back(rect{id, x, y, w, h});

		for (int i = x; i < x+w; i++) {
			for (int j = y; j < y+h; j++) {
				if (grid[i][j] == 1)
					total++;
				grid[i][j]++;
			}
		}
	}

	std::cout << total << "\n";

	for (auto const& r: input) {
		bool clean = true;
		for (int i = r.x; i < r.x + r.w; i++) {
			for (int j = r.y; j < r.y + r.h; j++) {
				if (grid[i][j] != 1)
					clean = false;
			}
		}
		if (clean)
			std::cout << r.id << std::endl;
	}
}
