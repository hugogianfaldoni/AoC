#!/bin/bash

if [ -z "$1" ]; then
	exit -1
fi

run() {
	if [ -e input/d$1 ]; then
		./d$1.bin < input/d$1
	else
		./d$1.bin
	fi
}

make d$1.bin && run $1
